import React, { useEffect, useState } from "react"
import { services } from "../../../services/services"
import "./menu.scss"

import { ReactComponent as XIcon } from "../../../assets/Images/xmark-solid.svg"
import { ReactComponent as LogoutIcon } from "../../../assets/Images/user-large-slash-solid.svg"
import { Link } from "react-router-dom"
import { useDispatch, useStore } from "react-redux"
import { logout } from "../../../redux/actions/auth"

const Menu = props => {
  const { menuClass, setMenuClass } = props

  const dispatch = useDispatch()
  const store = useStore()
  const [userData, setUserData] = useState(null)
  const [img, setImg] = useState(
    "https://cdn-icons-png.flaticon.com/512/1160/1160922.png"
  )

  const userName = `${userData ? userData?.display_name : "Cargando..."}`

  useEffect(() => {
    services.getMenu(store.getState(), setUserData, setImg)
  }, [store])

  return (
    <div className={`menu ${menuClass}`}>
      <XIcon
        className="menu__icon"
        onClick={() => setMenuClass("close-animate-menu")}
      />

      <div className="menu__container">
        <Link to="/home" className="menu__options">
          Home
        </Link>
        <Link to="/favs" className="menu__options">
          Favs
        </Link>
        <Link to="/albums" className="menu__options">
          Albums
        </Link>
      </div>

      <div className="menu__logout">
        <b className="menu__user">
          <img src={img} alt="UserImage" className="menu__user-image" />
          {userName}
        </b>
        <Link
          to="/"
          className="menu__logout-button"
          onClick={() => {
            dispatch(logout())
            localStorage.setItem(
              "GLOBAL_STATE",
              JSON.stringify(store.getState().auth)
            )
          }}
        >
          <LogoutIcon className="menu__logout-icon" />
          Cerrar sesión
        </Link>
      </div>
    </div>
  )
}

export default Menu
