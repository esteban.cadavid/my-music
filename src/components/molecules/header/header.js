import React, { useEffect, useState } from "react"
import { services } from "../../../services/services"
import "./header.scss"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faBars } from "@fortawesome/free-solid-svg-icons"
import { Link } from "react-router-dom"
import { ReactComponent as LogoutIcon } from "../../../assets/Images/user-large-slash-solid.svg"
import myImg from "../../../assets/Images/musicLogoC.png"
import { useDispatch, useStore } from "react-redux"
import { logout } from "../../../redux/actions/auth"

const Header = props => {
  const { setMenuClass, title, subtitle, menuClass } = props

  const dispatch = useDispatch()
  const store = useStore()
  const [userData, setUserData] = useState(null)
  const [img, setImg] = useState(
    "https://cdn-icons-png.flaticon.com/512/1160/1160922.png"
  )

  const userName = `${userData ? userData?.display_name : "Cargando..."}`

  useEffect(() => {
    services.getMenu(store.getState(), setUserData, setImg)
  }, [store])

  return (
    <header>
      <div className="header">
        <div className="header__invisible"></div>
        <div className="header__logo">
          <img src={myImg} className="header__imag" alt="Music logo" />
          <h1 className="header__name">My Music</h1>
        </div>

        <div className="header__menu">
          <Link to="/home" className="header__option">
            Home
          </Link>
          <Link to="/favs" className="header__option">
            Favs
          </Link>
          <Link to="/albums" className="header__option">
            Albums
          </Link>
        </div>

        <div className="header__logout">
          <b className="header__user">
            <img src={img} alt="UserImage" className="header__user-image" />
            {userName}
          </b>
          <Link
            to="/login"
            className="header__logout-button"
            onClick={() => {
              dispatch(logout())
              localStorage.setItem(
                "GLOBAL_STATE",
                JSON.stringify(store.getState().auth)
              )
            }}
          >
            <LogoutIcon className="header__logout-icon" />
          </Link>
        </div>

        <FontAwesomeIcon
          className="header__icon"
          icon={faBars}
          onClick={() => setMenuClass("open-animate-menu")}
        />
      </div>
      <h2 className="section">{title}</h2>
      <h3 className="subSection">{subtitle}</h3>
    </header>
  )
}

export default Header
