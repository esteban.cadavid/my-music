import React, { useEffect, useState } from "react"
import { useDispatch, useStore } from "react-redux"
import { login } from "../../redux/actions/auth"
import { services } from "../../services/services"
import ListTemplate from "../templates/list-template/list-template"

const Lists = () => {
  const store = useStore()
  const [recent, setRecent] = useState(null)
  const [favs, setFavs] = useState(null)
  const dispatch = useDispatch()

  useEffect(() => {
    if (!store.getState().auth.isLoggedIn) {
      dispatch(login())
      localStorage.setItem(
        "GLOBAL_STATE",
        JSON.stringify(store.getState().auth)
      )
    }
    services.getData(store.getState(), setRecent, setFavs)
  }, [dispatch, store])

  return (
    <ListTemplate
      songs={recent}
      favs={favs}
      title="Home"
      subtitle="Your recently played songs"
    />
  )
}

export default Lists
