import React, { useEffect, useState } from "react"
import { useStore } from "react-redux"
import { services } from "../../services/services"

import ListTemplate from "../templates/list-template/list-template"

const Favs = () => {
  const [favs, setFavs] = useState(null)

  const store = useStore()

  useEffect(() => {
    services.getFavs(store.getState(), setFavs)
  }, [store])

  return (
    <ListTemplate songs={favs} title="Favs" subtitle="Your favorite songs" />
  )
}

export default Favs
