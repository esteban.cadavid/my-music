import React from "react"
import { useEffect } from "react"
import LoginTemplate from "../templates/login-template/login-template"

const Login = () => {
  useEffect(() => {
    localStorage.setItem("actual", "track/5XqCz18k96K8tx6DAYM97i")
  }, [])

  return <LoginTemplate />
}

export default Login
