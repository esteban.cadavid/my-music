import React, { useEffect, useState } from "react"
import { useStore } from "react-redux"
import { services } from "../../services/services"
import ListTemplate from "../templates/list-template/list-template"

const Albums = () => {
  const [albums, setAlbums] = useState(null)

  const store = useStore()

  useEffect(() => {
    services.getAlbums(store.getState(), setAlbums)
  }, [store])

  return (
    <ListTemplate
      songs={albums}
      title="Albums"
      subtitle="Your playlists"
      isAlbum={true}
    />
  )
}

export default Albums
