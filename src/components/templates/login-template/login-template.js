import React from "react"
import Title from "../../atoms/title/title"
import { useDispatch } from "react-redux"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faSpotify } from "@fortawesome/free-brands-svg-icons"
import "./login-template.scss"
import constants from "../../../constants/constants"
import { firstLogin } from "../../../redux/actions/auth"

const LoginTemplate = () => {
  const dispatch = useDispatch()
  return (
    <div className="container">
      <Title />
      <a
        className="container__button"
        href={constants.SPOTIFY_REDIRECT}
        onClick={() => {
          dispatch(firstLogin())
        }}
      >
        <FontAwesomeIcon className="container__icon" icon={faSpotify} />
        Conectarse con Spotify
      </a>
    </div>
  )
}

export default LoginTemplate
