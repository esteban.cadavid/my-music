import React from "react"
import "./items-grid.scss"
import AlbumCard from "../album-card/album-card"
import SongCard from "../song-card/song-card"

const ItemsGrid = props => {
  const { songs, isAlbum, favs, title, setActualPlayer } = props
  const gridClass = `${isAlbum ? "album" : "songs"}`
  return (
    <ul className={"grid " + gridClass}>
      {!isAlbum &&
        songs &&
        songs.map((song, index) => {
          return (
            <li key={index}>
              <SongCard
                song={song?.track}
                index={index}
                title={title}
                favs={favs}
                setActualPlayer={setActualPlayer}
              />
            </li>
          )
        })}
      {isAlbum &&
        songs &&
        songs.map((song, index) => {
          return (
            <li key={index}>
              <AlbumCard song={song} setActualPlayer={setActualPlayer} />
            </li>
          )
        })}
    </ul>
  )
}

export default ItemsGrid
