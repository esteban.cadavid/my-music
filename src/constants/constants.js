const constants = {
  SPOTIFY_REDIRECT: `https://accounts.spotify.com/es-ES/authorize?client_id=5ebd49db6939414a916f9085492cb50b&scope=playlist-read-private,user-read-private,user-read-email,playlist-modify-public,user-library-read,user-library-modify,user-read-recently-played&redirect_uri=http://localhost:3000/home&response_type=token&show_dialog=true`,
  customerId: "5ebd49db6939414a916f9085492cb50b",
}

export default constants
