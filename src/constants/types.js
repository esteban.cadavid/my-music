export const types = {
  LOGIN: "[Auth] Login",
  LOGOUT: "[Auth] Logout",
  FIRST_LOG: "[Auth] First login",
}
