import React from "react"
import { Provider } from "react-redux"
import "./App.css"
import { BrowserRouter as Router } from "react-router-dom"
import Routing from "./utils/routes/routing"
import { store } from "./redux/store/store"

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Routing />
      </Router>
    </Provider>
  )
}

export default App
