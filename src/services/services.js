import axios from "axios"

const getHeaders = store => {
  const token =
    window.location.hash.substring(1).split("&")[0].split("=")[1] ??
    store.auth.token

  return {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  }
}

const getData = (store, setRecent, setFavs) => {
  let ids = ""
  const options = getHeaders(store)
  axios
    .get(
      "https://api.spotify.com/v1/me/player/recently-played?limit=50",
      options
    )
    .then(response => {
      if (response.status === 200) {
        setRecent(response?.data?.items)
        response.data.items.forEach(item => {
          ids = ids.concat(`${item.track.id}%2C`)
        })
        axios
          .get(
            `https://api.spotify.com/v1/me/tracks/contains?ids=${ids}`,
            options
          )
          .then(response => {
            if (response.status === 200) {
              setFavs(response?.data)
            }
          })
      }
    })
    .catch(() => {
      localStorage.clear()
      //window.location.href = "/"
    })
}

const getFavs = (store, setFavs) => {
  const options = getHeaders(store)
  axios
    .get("https://api.spotify.com/v1/me/tracks?limit=50", options)
    .then(response => {
      if (response.status === 200) {
        setFavs(response?.data?.items)
      }
    })
    .catch(() => {
      localStorage.clear()
      window.location.href = "/"
    })
}

const getAlbums = (store, setAlbums) => {
  const options = getHeaders(store)
  axios
    .get("https://api.spotify.com/v1/me/playlists", options)
    .then(response => {
      if (response.status === 200) {
        setAlbums(response?.data?.items)
      }
    })
    .catch(() => {
      localStorage.clear()
      window.location.href = "/"
    })
}

const putFavorite = (store, id) => {
  const data = JSON.stringify({ ids: [id] })
  const options = getHeaders(store)
  axios
    .put("https://api.spotify.com/v1/me/tracks?limit=50", data, options)
    .then(() => {
      window.location.href = window.location.href + ""
    })
    .catch(() => {
      localStorage.clear()
      window.location.href = "/"
    })
}

const deleteFavorite = (store, id) => {
  const options = getHeaders(store)
  axios
    .delete(`https://api.spotify.com/v1/me/tracks?ids=${id}`, options)
    .then(() => {
      window.location.href = window.location.href + ""
    })
    .catch(() => {
      localStorage.clear()
      window.location.href = "/"
    })
}

const getMenu = (store, setUserData, setImg) => {
  const options = getHeaders(store)
  axios
    .get("https://api.spotify.com/v1/me", options)
    .then(response => {
      if (response.status === 200) {
        setUserData(response?.data)
        setImg(
          `${
            response?.data?.images[0]
              ? response?.data.images[0].url
              : "https://cdn-icons-png.flaticon.com/512/1160/1160922.png"
          }`
        )
      }
    })
    .catch(() => {
      localStorage.clear()
      window.location.href = "/"
    })
}

export const services = {
  getData,
  getFavs,
  getAlbums,
  putFavorite,
  deleteFavorite,
  getMenu,
}
