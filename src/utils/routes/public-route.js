import React from "react"
import { useStore } from "react-redux"
import { Navigate } from "react-router-dom"

const PublicRoute = ({ children }) => {
  const auth = useStore().getState().auth

  return !auth.isLoggedIn ? children : <Navigate to="/home" />
}

export default PublicRoute
