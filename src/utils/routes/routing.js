import React from "react"
import { Route, Routes, Navigate } from "react-router-dom"
import Albums from "../../components/pages/albums"
import Favs from "../../components/pages/favs"
import Lists from "../../components/pages/lists"
import Login from "../../components/pages/login"
import PrivateRoute from "./private-route"
import PublicRoute from "./public-route"

const Routing = () => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="/login" />} />
      <Route
        path="/login"
        element={
          <PublicRoute>
            <Login />
          </PublicRoute>
        }
      />
      <Route
        path="/home"
        element={
          <PrivateRoute>
            <Lists />
          </PrivateRoute>
        }
      />
      <Route
        path="/favs"
        element={
          <PrivateRoute>
            <Favs />
          </PrivateRoute>
        }
      />
      {}
      <Route
        path="/albums"
        element={
          <PrivateRoute>
            <Albums />
          </PrivateRoute>
        }
      />
    </Routes>
  )
}

export default Routing
