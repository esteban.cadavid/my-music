import React from "react"
import { useStore } from "react-redux"
import { Navigate } from "react-router-dom"

const PrivateRoute = ({ children }) => {
  const auth = useStore().getState().auth

  return auth.token || window.location.hash ? (
    children
  ) : (
    <Navigate to="/login" />
  )
}

export default PrivateRoute
