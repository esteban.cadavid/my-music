import { types } from "../../constants/types"

/* const initialState = {
  token: "",
  isLoggedIn: false,
} */

const globalState = localStorage.getItem("GLOBAL_STATE")

const initialState = globalState
  ? JSON.parse(globalState)
  : {
      token: null,
      isLoggedIn: null,
    }

export const authReducer = (state = initialState, action) => {
  switch (action?.type) {
    case types.LOGIN:
      return {
        token: action.token,
        isLoggedIn: true,
      }
    case types.LOGOUT:
      return {
        token: null,
        isLoggedIn: false,
      }

    case types.FIRST_LOG:
      return {
        token: null,
        isLoggedIn: true,
      }
    default:
      return state
  }
}
