import { createStore, combineReducers, applyMiddleware, compose } from "redux"
import { authReducer } from "../reducers/auth-reducer"
import thunk from "redux-thunk"

const globalState = localStorage.getItem("GLOBAL_STATE")

const initialState = globalState
  ? JSON.parse(globalState)
  : {
      token: null,
      isLoggedIn: null,
    }

const composeEnhancers =
  (typeof window !== "undefined" &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose

const reducers = combineReducers({
  auth: authReducer,
})

export const store = createStore(
  reducers,
  initialState,
  composeEnhancers(applyMiddleware(thunk))
)
