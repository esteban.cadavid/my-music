import { types } from "../../constants/types"

export const login = () => {
  if (window.location.hash) {
    const token = window.location.hash.substring(1).split("&")[0].split("=")[1]
    window.location.hash = ""
    return {
      type: types.LOGIN,
      token,
    }
  }
}

export const logout = () => {
  return {
    type: types.LOGOUT,
  }
}

export const firstLogin = () => {
  return {
    type: types.FIRST_LOG,
  }
}
